PImage source;
PImage sortewd;

static final String inputFileDir = "./";
static final String inputFileName = "file_name";
static final String inputFileFormat = ".jpg";
static char mode = 'M';		//definir "M" para maior ou "m" para menor
static final boolean transform = true; //calls color transformation in sort function
static final boolean after = false; //transform may be called after of before array sort
static final int scaleY = 500; //vertical length
static final boolean save_frames = false; //press s to save

//configure limiar here//
int limiar = 1;

//static final int RED=65536, GREEN=256, BLUE=1; //color is stored as (hex)#AARRGGBB

void settings(){
	source = loadImage(inputFileDir+inputFileName+inputFileFormat);	//function description says not to do this
	size(source.width*scaleY/source.height, scaleY);	//can't do this without line above
}

void setup(){
//  size();
	println("setup");
/*  size (source.width, source.height);
    can't use variables to define size;
    must be first line inside setup();
*/
	//source = loadImage(inputFileDir+inputFileName+inputFileFormat);
	sortewd= createImage(source.width, source.height, RGB);
	
	source.resize(0, scaleY);
	sortewd.resize(0, scaleY);	

	horizontalSort();
}

/*incrementa limiar & salva imagens*/
void mouseWheel(MouseEvent event){
  int e = event.getCount();
	String outputFileName = "resultados/"+inputFileName+limiar+".jpg";
	
	if (save_frames)
		save(outputFileName);
	limiar += e;
	horizontalSort();
	println("limiar = ", limiar);
}

void keyReleased(){
  if (key == 'm' || key == 'M'){	//MUST TEST THIS
  	mode = key;
  println(mode);
  }
  if (keyCode != UP && keyCode != DOWN)
  	return;
  int e;
  	if (keyCode == UP)
  		e = 1;
  	else	//keyCode == DOWN
  		e = -1;
  String outputFileName = "resultados/"+"ibagem-"+limiar+".jpg";
  
  if (key == 's')
    saveFrame(outputFileName);
  limiar += e;
  horizontalSort();
  println("limiar = ", limiar);
}

void horizontalSort(){
	source.loadPixels();
	sortewd.loadPixels();  
	int j;
	for (int i = 0; i< source.pixels.length; i += j){
		/*for (j = 1; (i+j) < source.pixels.length && abs(source.pixels[i+j -1] - source.pixels[i+j]) >= limiar*(RED+GREEN+BLUE); j++){
    	issue RED value smashes GREEN and BLUE values when calculating difference [RED has priority]
    */
  	boolean RGBdif = true;
  	for (j = 1; (i+j) < source.pixels.length && RGBdif; j++){
			if (mode == 'm')
    		RGBdif = abs(red(source.pixels[i+j -1]) - red(source.pixels[i+j])) <= limiar && abs(green(source.pixels[i+j -1]) - green(source.pixels[i+j])) <= limiar && abs(blue(source.pixels[i+j -1]) - blue(source.pixels[i+j])) <= limiar;
      else //mode = 'M'
      	RGBdif = abs(red(source.pixels[i+j -1]) - red(source.pixels[i+j])) >= limiar && abs(green(source.pixels[i+j -1]) - green(source.pixels[i+j])) >= limiar && abs(blue(source.pixels[i+j -1]) - blue(source.pixels[i+j])) >= limiar;

  	/*feature: change >= for <=  in abs difference  */
		}
//    println(j);
		color[] newArray = (color[]) subset (source.pixels, i, j);
//transform function is a test with some transformations try AFTER or BEFORE sorting)
	if (transform && after) {
		transform (newArray);
	}
		newArray = sort (newArray);
  if (transform && !after) {
		transform (newArray);
  }
		for (int l = 0; l < j; l++){
			sortewd.pixels[i + l] = newArray[l];
		}
	}
	sortewd.updatePixels();
	
}

void transform (color[] newArray){
  if (newArray.length > 0)	//set limiar
		for (int i = 0; i < newArray.length; i++ )
 			newArray[(i*22)%newArray.length] = color((red(newArray[i])+green(newArray[i]))%256,(green(newArray[i])+blue(newArray[i]))%256,(blue(newArray[i])+red(newArray[i]))%256);
}

void draw(){
  image (sortewd, 0, 0);
//  image (source, 0, 0);

}
